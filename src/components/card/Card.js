import React from "react";
import Header from '../header/Header'
import Button from "../button/Button";
import "antd/dist/antd.css"
import { Card, Col, Row } from 'antd';


const { Meta } = Card;

const card = (props) => {
  const { title, description, imgSrc, imgAlt, btnText, howMany} = props;
  const elements = [];

  for(let i = 0; i < parseInt(howMany); i++){
    var cardTitle = title + ' ' + (i+1).toString();
    elements.push(
      <Col span={8}> 
      <Card
        hoverable
        style={{ width: 240 }}
        cover={<img alt={imgAlt} src={imgSrc} />}
      >
        <Meta title={cardTitle} description={description} />

        <Button 
          type="primary"
          text="Klik Sini, Gan!"
        />
      </Card>
      </Col>
    )
  }


  return (
    <div className="site-card-wrapper">
    <Row gutter={16}>
      {elements}
    </Row>
  </div>
  )
}

export default card;
