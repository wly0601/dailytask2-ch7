import "antd/dist/antd.css"
import { Button } from 'antd';

const button = (props) => {
  const {type, text} = props;
  return (
      <Button type={type} block>
        {text}
      </Button>
    )
}

export default button