module.exports = {
	greetings(DateInput){
	const dateNow = new Date(DateInput)
		if(dateNow.getHours() < 5){
			return "Good Evening!";
		} else if(dateNow.getHours() < 12){
			return "Good Morning!";
		} else if(dateNow.getHours() < 18){
			return "Good Afternoon!";
		} else {
			return "Good Evening!";
		}
	},
}