import Card from './components/card/Card';
import Header from './components/header/Header';

import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import Time from './components/date/Time.js';

function App() {
  const date = Date(Date.now);
  const goodWhat = Time.greetings(date);
  const greetings = `Hello, ${goodWhat} Welcome to Class of `
    return ( 
      <div className = "App" >
        < Header title = "FSW"
          greetings = {greetings}
          number = "13"
          background = "grey"
        />

        < Card title = "Card"
          description = "Lorem ipsum dolor sit amet"
          btnText = "Klik Disini Bosku!"
          imgSrc = "https://placeimg.com/320/240/any"
          imgAlt = "Hello"
          howMany = "9" 
        />
      </div>
    );
}

export default App;